#include<highgui.h>
#include "opencv2/imgproc/imgproc.hpp"
#include<stdio.h>
#include <iostream>
#include<cv.h>
#include<math.h>
using namespace cv;
using namespace std;
int size_lines;
int counter=0,gencounter=0;
int eps=10;
float eps1=10;
int width=6;
int rad_width=2;
IplImage *fin;
/*
Defining node where helm indicates whether it is a line or a circle.start and end the the starting and endiing points resp.
*/
typedef struct node
{
char helm;
Point start;
Point end;
node *next;
node *prev;
}node;
typedef struct list
{
node *head;
int count;
}list;
list *l;
int initcntr=0,endcntr=0;
/*
Creating new list
*/
void eliminate_line(node *temp1)
{
node *aux2=temp1->next;
node *aux=temp1->prev;
if (aux!=NULL && aux2!=NULL)
{
aux->next=aux2;
aux2->prev=aux;
}
else if (aux==NULL && aux2!=NULL)
{
aux2->prev=NULL;
}
else if (aux!=NULL && aux2==NULL)
{
aux->next=NULL;
}
free(temp1);
}
float slope(Point p1,Point p2)
{
float slp=(float)(p2.y-p1.y)/(p2.x-p1.x);
return slp;
}
int parallel_dist(Point p1,Point p2,Point p3,Point p4)
{
//printf("Points got-(%d,%d) (%d,%d)\n",p1.x,p1.y,p2.x,p2.y);
//printf("Points 2 got-(%d,%d) (%d,%d)\n",p3.x,p3.y,p4.x,p4.y);
if (p1.x==p2.x)
return fabs(p3.x-p1.x);
else
return fabs(p3.y-p1.y);

}
int dist2(Point p1,Point p2)
{
int dist2=pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2);
return dist2;
}
void create_new_list(char comp,Point start,Point end)
{

node *tnew=(node *)malloc(sizeof(node));
l=(list *)malloc(sizeof(list));

if (tnew!=NULL)
{

tnew->helm=comp;
tnew->start=start;
tnew->end=end;

tnew->next=NULL;
tnew->prev=NULL;
l->count=1;
l->head=tnew;
counter=1;
}
}
/*
Adding to the nodes
*/
void add_new_node(char comp,Point start,Point end)
{
node *tnew=(node *)malloc(sizeof(node));
if (tnew!=NULL)
{
tnew->helm=comp;
tnew->start=start;
tnew->end=end;
tnew->next=l->head;
l->head->prev=tnew;
tnew->prev=NULL;
l->head=tnew;
l->count++;

}
}
/*
Printing nodes
*/
void print_node(node *n) 
{
printf("Node - ");
printf("%c\t",n->helm);
printf("(%d,%d) ",n->start.x,n->start.y);
printf("(%d,%d)",n->end.x,n->end.y);
printf("\n");
} 
/*
Printing list
*/
void print_list() 
{

node *temp=l->head;
//cvZero(fin);
while (temp!=NULL)
{
if (temp->helm=='l')
cvLine(fin,temp->start,temp->end,Scalar(255,0,0),1);
else
{
Point centre;
centre.x=(temp->start.x+temp->end.x)/2;
centre.y=temp->start.y;
int rad=(temp->start.x-temp->end.x)/2;
rad=abs(rad);
cvCircle(fin,centre,rad,Scalar(255,0,0),1);
}
print_node(temp);
temp=temp->next;
}
}
void remover(node *temp)
{
if (temp->next==NULL)
free(temp);
else 
{
remover(temp->next);
free(temp);
}
}

/*
Creating the linked list
*/
void par_elm()
{
int slope_eps=10;
node *temp=l->head;
node*temp1=l->head;
while (temp!=NULL)
{
if (temp->helm=='c')
{
temp=temp->next;
continue;
}
while (temp1!=NULL)
{
if (temp==temp1)
{
temp1=temp1->next;
continue;
}
if (temp1->helm=='c')
{
temp1=temp1->next;
continue;
}

if ((slope(temp->start,temp->end)-slope(temp1->start,temp1->end)<=slope_eps) || ((temp->start.x==temp->end.x) && (temp1->start.x==temp1->end.x)))
{
if (parallel_dist(temp->start,temp->end,temp1->start,temp1->end)<width && parallel_dist(temp->start,temp->end,temp1->start,temp1->end)>0)
{
if (dist2(temp->start,temp->end)<dist2(temp1->start,temp1->end))
{
node *swap=temp;
temp=temp1;
temp1=swap;
}
printf("Parallel Distance-%d\n",parallel_dist(temp->start,temp->end,temp1->start,temp1->end));
//cvWaitKey(0);

temp1=temp1->next;
eliminate_line(temp1->prev);
}
else
temp1=temp1->next;
}
else
temp1=temp1->next;
}
temp1=l->head;
temp=temp->next;
}

}

void rad_elm(vector<Vec3f> circles)
{
node *temp;
int slope_eps=1;
for( size_t i = 0; i < circles.size(); i++ )
{
Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
int radius = cvRound(circles[i][2]);
int dist_elm=rad_width+radius;
temp=l->head;

while(temp!=NULL)
{
if (temp->helm=='c')
{
temp=temp->next;
continue;
}



if (dist2(temp->start,temp->end)<=slope_eps && (dist2(temp->end,center)>=(radius*radius)))
{
temp=temp->next;
eliminate_line(temp->prev);
continue;
}



if (dist2(temp->start,center)<(radius*radius) || dist2(temp->end,center)<(radius*radius))
{
temp=temp->next;
eliminate_line(temp->prev);
}
else
temp=temp->next;
}
}
}
void give_items(char himg,Point start,Point end)
{
int flag=0;
if (counter==0)
create_new_list(himg,start,end);
else
add_new_node(himg,start,end);


initcntr=endcntr=l->count;

//elm();


}
int threshholder(IplImage *img)
{
long int sum=0;
int avg;
for (int y=0;y<img->height;y++)
{
uchar *ptr=(uchar *)(img->imageData + y*img->widthStep);
for (int x=0;x<img->width;x++)
sum+=ptr[x];
}
avg=sum/(img->height*img->width);
return avg;
}
int main(int argc, char** argv)
{
int x1,y1,x2,y2;
int ref=100;

/*
img is the passed image,fin is used as the image for outlines
*/
IplImage *img=cvLoadImage(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
//cvSmooth(img,img,CV_GAUSSIAN,7,7);
fin=cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);
cvZero(fin);
/*
Morphological operations are performed on the image
*/

cvThreshold(img,img,255-threshholder(img),255,CV_THRESH_BINARY);
cvSmooth(img,img);
Mat src=Mat(img);
Mat mfin;
Mat dst, cdst,gray,cgray;
/*
Edges are detected
*/
Canny(src, dst, 130, 250, 3);
imshow("Cannied image",dst);
gray=Mat(src);
cvtColor(dst, cdst, CV_GRAY2BGR);
cvtColor(gray, cgray, CV_GRAY2BGR);
vector<Vec4i> lines;
vector<Vec3f> circles;
namedWindow( "comp", 1 );
/*
Using probabilistic hough transform to get the lines
*/
HoughLinesP(dst, lines, 1, CV_PI/180, 5, 10, 0 );
size_lines=lines.size();
for( int i = 0; i < size_lines; i++ )
{
Vec4i l = lines[i];
x1=l[0];
y1=l[1];
x2=l[2];
y2=l[3];

//cvLine( fin, Point(x1, y1), Point(x2, y2), Scalar(0,0,255), 1, CV_AA);
give_items('l',Point(x1,y1),Point(x2,y2));
line( cdst, Point(x1, y1), Point(x2, y2), Scalar(0,0,255), 1, CV_AA);
}
/*
Using hough circles to get the circles.
*/
HoughCircles(gray, circles, CV_HOUGH_GRADIENT,2, gray.rows/2, 200, 50,0,gray.rows/5 );
for( size_t i = 0; i < circles.size(); i++ )
{

Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
int radius = cvRound(circles[i][2]);
// draw the circle center
circle( cgray, center, 3, Scalar(0,255,0), -1, 8, 0 );
// draw the circle outline
cvCircle( fin, center, radius, Scalar(0,0,255), 1, 8, 0 );
give_items('c',Point(center.x-radius,center.y),Point(center.x+radius,center.y));
circle( cgray, center, radius, Scalar(0,0,255), 1, 8, 0 );
}
printf("Before\n");
print_list();

par_elm();
rad_elm(circles);

print_list();
/*
Displaying the src,lines,circles and the outline
*/
namedWindow( "circles", 1 );

imshow( "circles", cgray );
//combine(lines);
imshow("source", src);
imshow("detected lines", cdst);
//cvSmooth(fin,fin);
mfin=Mat(fin); 
imshow("outline", mfin);
cvDestroyWindow("comp");      
cvWaitKey(0);


node *temp2=l->head;
IplImage *genimg=cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,3);
cvZero(genimg);
int counter3=0;
while (temp2!=NULL)
{

counter3++;
if (temp2->helm=='l')
cvLine(genimg,temp2->start,temp2->end,Scalar(0,255,0),1);
else
{
Point centre;
centre.x=(temp2->start.x+temp2->end.x)/2;
centre.y=temp2->start.y;
int rad=(temp2->start.x-temp2->end.x)/2;
rad=abs(rad);
cvCircle(genimg,centre,rad,Scalar(0,255,0),1);
}
temp2=temp2->next;
}
printf("Fin-%dStart-%d Eliminated %d\n",l->count-counter3,l->count,counter3);
cvShowImage("gen",genimg);
cvWaitKey(0);
remover(l->head);
counter=0;
return 0;
}

